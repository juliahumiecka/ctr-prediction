# CTR Prediction

Prediction of Click-Through Rate value (which is important metric for evaluating ad performance) using data from Avazu CTR prediction Contest ([available here](https://www.kaggle.com/c/avazu-ctr-prediction/data)). Testing out Random Forests and Decision Tree with AdaBoost algorithms.

Project contains 2 notebooks - one used for data analysis, second - for preparation and normalization of data, creating and training chosen ML models.
